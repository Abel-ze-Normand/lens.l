# lens.l -- Lens abstraction library: pattern and utilities
#
# The MIT License (MIT)
#
# Copyright (c) 2019 Nail Gibaev

(de l-make-idf @
   # identity constructor
   (pass cons 'idf) )

(de l-make-constf @
   # constant constructor
   (pass cons 'constf) )

# fmap: (a -> b) -> f a -> f b
(de l-fmap (Fun C)
   # functor mapping implementation
   # ARGS:
   #   Fun -- function for modifying FOCUSED value
   #   C -- cons pair with functor symbol in CAR and value in CDR
   # RETURN:
   #   changed value by Fun and wrapped in constructor
   (case (car C)
      (constf C)
      (idf (l-make-idf (Fun (cdr C)))) ) )

(de lens-map (Fun L S)
   # value at focus mapping function
   # ARGS:
   #   Fun -- function for modifying FOCUSED value
   #   L -- lens instance
   #   S -- WHOLE value
   # RETURN:
   #   apply Fun on focused value by lens L for S WHOLE value and return S WHOLE value changed
   (let Fmap (list
                '(A)
                (list 'l-fmap (lit Fun) (list 'l-make-idf 'A)) )
      # Fmap function manages for unwrapping FOCUSED value by lens, passing it to ``fmap`` and
      # wrapping it back

      # lens execution result will be wrapped in one of the constructors so unwrap them manually
      (cdr (L Fmap S)) ) )

(de lens-get (L S)
   # function for value retrieval from lens focus
   # ARGS:
   #   L -- lens instance
   #   S -- WHOLE value
   # RETURN:
   #   FOCUSED value provided by lens L for S WHOLE value
   (let Fmap (list
                '(A)
                (list 'l-fmap 'NOFUN (list 'l-make-constf 'A)) )
      (cdr (L Fmap S)) ) )

(de lens-put (L A S)
   # function for value insertion at lens focus
   # ARGS:
   #   L -- lens instance
   #   A -- value to put at FOCUS point
   #   S -- WHOLE value
   # RETURN:
   #   WHOLE value with new value A at FOCUS of lens L
   (let Fmap (list
                (list '_IGNORE)
                # because there is no need to manage data which will be placed at lens FOCUS
                # so just return data to insert into FOCUS
                (list 'bind (lit (env '(A)))
                   'A ) )
      (lens-map Fmap L S) ) )

(de lens-iso-1to1 (L1 S1 L2 S2)
   # lens isomorphism building function. Manages to extract from S1 data structure
   # with lens focus L1 and put it in S2 data structure at lens focus L2
   # ARGS:
   #   L1 -- lens instance FROM
   #   S1 -- WHOLE value FROM
   #   L2 -- lens instance TO
   #   S2 -- WHOLE value TO
   # RETURN:
   #   WHOLE value S2 with set
   (lens-put L2 (lens-get L1 S1) S2) )

(de lens-c Lenses
   # lens composition function. chain multiple lenses into one which can focus in
   # very nested point in data structure
   # ARGS:
   #   Lenses -- list of lens instances
   # RETURN:
   #   instance of lens composition with identical interface as plain lens
   (ifn (cdr Lenses)
      # if single lens is provided then use it without further composition
      (eval (car Lenses))
      (let Lenses (reverse Lenses)
         # lenses order must be reversed
         # bcs composition will construct new function from _inside out_

         (list '(Fun S)
            # in this implementation lenses are build manually as one big clojure.
            # its possible at the moment of call of ``lens-c`` function
            # because all lenses are present from start (except Fmap or WHOLE value)
            (recur (Lenses Fun)
               (let Lens (eval (car Lenses))
                  # pick lens
                  (ifn (cdr Lenses)
                     (list (lit Lens) (lit Fun) 'S)
                     # when lenses list is exhaused -- just call last one
                     (let NextFun (list
                                     '(X)
                                     (list
                                        (lit Lens)
                                        (or (lit Fun) 'Fun) 'X ) )
                        # construct function to pass it further into next lens in list.
                        # use Fun from previous iteration, but on first iteration Fun symbol will
                        # be NIL so just pass Fun as symbol (from arg). then Fmap wrapper will be passed for first lens
                        (recurse (cdr Lenses) NextFun S) ) ) ) ) ) ) ) )

(de lens-p Lenses
   # lens product function. setup multiple different lenses to focus at different points
   # of WHOLE value and extract or map them on given focus
   # ARGS:
   #   Lenses -- list of lens instances
   # RETURN:
   #   instance of lens product with identical interface as plain lens
   (ifn (cdr Lenses)
      # if single lens is provided then there is no need to build other stuff
      (eval (car Lenses))

      (list '(Fun S)
         # in this implementation lenses are build manually as one big clojure.
         # its possible at the moment of call of ``lens-p`` function
         # because all lenses are present from start (except Fmap or WHOLE value)
         (list 'bind (list 'env (lit (list 'S 'Fun)))
            # track current environment
            (let (GetP (make  # getter list
                          (link 'list)
                          (for L Lenses
                             (link (list 'lens-get (lit (eval L)) 'S)) ) )
                  PutP (list '(Xs)  # setter chain
                          (list 'let 'Lenses (lit (mapcar eval Lenses))
                             (list 'bind (list 'env (lit (list 'S 'Xs)))
                                '(for L Lenses
                                    (setq S (lens-put L (car Xs) S))
                                    (setq Xs (cdr Xs)) )
                                'S ) ) ) )
                  # Xs here contains list of FOCUSED values
                  # (in order of lenses in product, from left to right)
               (list 'l-fmap (lit PutP) (list 'Fun GetP)) ) ) ) ) )

(de fconcat (FmapRes1 FmapRes2)
   # helper function to accumulate results with ``traverse`` lens.
   # FmapRes1 contains result of ``l-fmap`` for current iteration,
   # FmapRes2 is accumulator
   (cons (car FmapRes1) (cons (cdr FmapRes1) (cdr FmapRes2))) )

(de traverse-lens ()
   # lens for focusing at each element of list. keep in mind that ``traverse-lens`` is not
   # well behaved
   # ARGS:
   #   None
   # RETURN:
   #   instance of lens
   '((F S)
     (let Acc (l-make-constf NIL)
        (for I S
           (setq Acc
              (fconcat
                 (l-fmap '((X) X) (F I))
                 Acc ) ) )
        (cons (car Acc) (reverse (cdr Acc))) ) ) )

(de l-make-lens (Omega Env . LensBody)
   # lens building helper function. Helps with management of environment
   # ARGS:
   #   Omega -- OMEGA value for replacing NIL value, if focus is failed
   #   LensBody -- lens function itself
   # RETURN:
   #   constructed lens instance
   (list
      (car LensBody)
      (list 'bind (lit (conc (env '(Omega)) Env ))
         (cadr LensBody) ) ) )

(de car-lens (Omega)
   # lens for focusing at CAR
   # ARGS:
   #   Omega -- OMEGA value for replacing NIL value, if focus is failed
   # RETURN:
   #   instance of lens
   (l-make-lens Omega NIL
      (F S)
      # F -- function for wrap-apply-unwrap management
      # S -- WHOLE value
      (let Rst (cdr S)
         (l-fmap
           '((V) (cons V Rst))
           (F (or (car S) Omega)) ) ) ) )

(de cdr-lens (Omega)
   # lens for focusing at CDR
   # ARGS:
   #   Omega -- OMEGA value for replacing NIL value, if focus is failed
   # RETURN:
   #   instance of lens
   (l-make-lens Omega NIL
      (F S)
      (let Car (car S)
         (l-fmap
            '((V) (cons Car V))
            (F (or (cdr S) Omega)) ) ) ) )

(de at-lens (I Omega)
   # lens for focusing at given index
   # ARGS:
   #   I -- index of list for focusing. index access follows
   #        the same behaviour as ``get`` and ``place`` Picolisp functions
   #   Omega -- OMEGA value for replacing NIL value, if focus is failed
   # RETURN:
   #   instance of lens
   (l-make-lens Omega (env '(I))
      (F S)
      (l-fmap
         '((V) (place I S V)) #[NGibaev 16.05.19] probably should wrap into (need) call? possible inefficient memory consumption
         (F (or (get S I) Omega)) ) ) )

(de assoc-pop (Key L)
   (make
      (for I L
         (ifn (= (car I) Key) #[NGibaev 15.05.19] possible bug: removes all occurences of key in assoc
            (link I) ) ) ) )

(de assoc-at-lens (Key Omega)
   # lens for focusing at specific value of assoc list
   # ARGS:
   #   Key -- key in assoc list for focusing
   #   Omega -- OMEGA value for replacing NIL value, if focus is failed
   # RETURN:
   #   instance of lens
   (l-make-lens Omega (env '(Key))
      (F S)
      (l-fmap
         '((V) (conc
                  (list (cons Key V))
                  (assoc-pop Key S) ) ) #[NGibaev 19.05.19] probably for performance reasons I should remove ``assoc-pop`` assertion?
         (F (or (cdr (assoc Key S)) Omega)) ) ) )