# lens.l - Data accessors in pure functional way for PicoLisp

Library with Lenses, utilities for their definition, composition and execution. Please read [EXPLAIN.md](EXPLAIN.md) for lenses motivation, implementation and applications.

1. [Requirements](#requirements)
2. [Getting Started](#getting-started)
3. [Introduction](#introduction)
4. [Usage](#usage)
5. [Examples](#examples)
6. [Testing](#testing)
7. [Contributing](#contributing)
8. [License](#license)

# Requirements

* PicoLisp 64-bit v3.1.11+ (Tested up to PicoLisp v18.9.5)

# Getting Started

1. Include `lens.l` by copying file manually from this repository or by git submodule and load it
2. Check [examples](#examples) below

# Introduction

If you are not familiar with lenses, you should check some resources first:

* [Polymorphic Update with van Laarhoven Lenses](http://r6.ca/blog/20120623T104901Z.html)
* [A Modern History of Lenses](http://data.tmorris.net/talks/modern-history-of-lenses/f1816a123cfc8d6f7cc6a586ca851da4c02452f7/lenses.pdf)
* [Lenses: viewing and updating data structures in Haskell](https://www.twanvl.nl/files/lenses-talk-2011-05-17.pdf)

But if you have very little Haskell background (just like me!) I will try to explain primary purpose of this abstraction. If we step back into capabilities of imperative languages then you can remember that access operations like this are nice and easy:

```python
# get nested value
obj.field_a.nested_field_b

# set nested value
obj.field_a.nested_field_b = "new value"

# update nested value
obj.field_a.nested_field_b = foo(obj.field_a.nested_field_b)
```

Though that this code is not strictly following [Law of Demeter](https://en.wikipedia.org/wiki/Law_of_Demeter), more of it this semantics is not possible for functional languages, because all objects and nested ones in particular are immutable. So, in functional languages even if access to read in nested structures is possible its forbidden to update them with "in place" semantics. This is very handy in general (bcs. violation of functional transparency is forbidden) but if we actually have to change value in deeply nested data structure then we will have to update nested structure and rebuild all of them along the path:

```elixir
iex(1)> s = %{field_a: %{nested_field_b: 1} }
%{field_a: %{nested_field_b: 1}}

iex(2)> %{s | field_a: %{s[:field_a] | nested_field_b: 2}}
%{field_a: %{nested_field_b: 2}}
```

This problem become more painful as more nested structures are. Also don't forget about cases with updating objects inside of nested lists where getting/updating/setting of nested value is impossible without mapping of nested list and rebuilding it again for storing in attribute (or even attempting to update value by updating element of list at specific index which is not always possible too). And there is much more examples when this became more and more complex, ugly and error-prone not only for functional languages but for imperative ones too.

One of the techniques for abstracting this problem is Lenses. Theirs name is much self-explanatory - _lens_ is an abstraction which is don't change something in formatted data by itself rather than _focusing at specific point(s)_ of formatted object. You may think of it just as regular lens from optics where one have _focus point_ at some distance from center of the lens. More of it: _lenses_ as an abstraction also may compose like their counterpart from optics to modify focus point distance.

Let's for example review lens for accessing CAR in list for LISP language which is named `car-lens`. As its name suggests, this specific lens focuses at specific point in list - at CAR of the list. This lens is polymorphic from perspective of input data: CAR lens will work with any list and will point only at CAR for every instance of input data. You can relate such mindset with i.e. python where indexer operator works with every list. Same mindset should be followed for each lens.

```
   (car-lens)
       |
 LENS FOCUS POINT
       |
       V
(list I1 I2 ... IN)
```

But lens by itself is not very useful - we have only lens produced by lens constructor. Here we can use special utility functions which are actually manage our data by using provided lens instance. So if we want to read CAR of the list, we should call _GET_ function to get value from specific list:

```picolisp
(lens-get (car-lens) '(1 2 3))
# -> 1
```

The same semantics but with different behaviour is available for CDR lens. It is lens, which focuses at CDR of its given list (its not quite true: it can focus at CDR of ANY cell and list in particular).

```picolisp
(lens-get (cdr-lens) '(1 2 3))
# -> (2 3)
```

If we need to replace/update data at specific point there are available _PUT_ and _MAP_ utilities:

```picolisp
(lens-put (cdr-lens) '(4 5) '(1 2 3))
# -> (1 4 5)

(lens-put (car-lens) 0 '(1 2 3))
# -> (0 2 3)

(lens-map '((L) (mapcar inc L)) (cdr-lens) '(1 2 3))
# -> (1 3 4)
# lens focused at CDR: so L in lambda is bound to '(2 3).
# (mapcar) iterates and maps function (inc) over each element
# of L and result is returned at place where
# lens is focused: back to CDR of initial data structure
```

Last argument with initial data for _GET/PUT/MAP_ functions is called _whole_ value in literature and abbreviated as _S_. _Value at focus point_ is shortened as _focus value_ and abbreviated as _A_.

Lenses follow strict algebraic rules to be called _well behaved_ lenses:

1) _GetPut Law_: `S = PUT lens (GET lens S) S`. If we retrieve data with some lens with GET and we PUT result of focusing with GET to put focused value with the same lens back then result should be equal to initial S. In other terms, `GET . PUT <=> id`.
2) _PutGet Law_: `A = GET lens (PUT lens A S)`. If we store value A into data structure S with some lens and result of PUTting we will use to GET with same lens as we used for PUT then we result of GET should be A.
3) _PutPut Law_: `A2 = GET lens (PUT lens A2 (PUT lens A1 S))`. If we consecutively PUT value at focus point multiple times with same lens, then at the end of this chain `PUT_1 . PUT_2 . ... PUT_n` value of lens focus point should be equal to last PUT value.

To be shorter for lens laws: lens should preserve and respect initial _whole_ value structure in order to be truly useful, composable and safe to use. This restriction is dictated by its Functor nature, which follows declaration: `fmap: (a -> b) -> f a -> f b`. So, functor always restores object structure upon modification of its contents. More of discussion for this topic is available in [EXPLAIN.md](EXPLAIN.md).

But if we need to access data in deeper place in _whole_ value, previously described lenses are not sufficient. Lets say, if we want to update CAAR of list, what lens should we use? Though its possible to declare lens constructor which will access only CAAR, its not abstract enough. With such mindset, we will have to implement lens for access to CAAR, CAADR, CADR etc. Its tedious and wrong. But inStead You should use _lens composition_ utility which "glues" multiple lenses to focus at much deeper point. See lens composition same as function composition in mathematics.

```picolisp
# its not required to call lens constructor at each call: you can
# call them once and store somewhere, so you will not re-build
# them at runtime
(let CaarLens (lens-c (car-lens) (car-lens))
    (lens-get CaarLens '((1))) )
# -> 1

(let CadrLens (lens-c (cdr-lens) (car-lens))
    (lens-put CadrLens 0 '(1 2 3)) )
# -> (1 0 3)
```

Lens composition function `(lens-c)` principle of work is not very much complicated to understand. Its collects all provided lenses to build one bigger lens! And lens composition of well-behaved lenses is also is well behaved lens so lens constructed by lens composition of multiple smaller lenses is also well-behaved (i.e. preserves inner structure of objects). So with `(lens-c)` you actually able to change value which lies deep within whole data without need to rebuild whole data along the navigation path.

But there is also lens utility which also combines multiple lenses in order to access to _multiple_ locations at once and gather/update them simultaneously: its called lens product. Lets say if we want to access to multiple focus points to gather values #1 and #3 in list:

```picolisp
(let (CarLens (car-lens)
      CdrLens (cdr-lens)
      CaddrLens (lens-c CdrLens CdrLens CarLens) )
    (lens-get (lens-p CarLens CaddrLens) '(1 2 3 4)) )
# -> (1 3)
```

Check this picture if you do not understand what happened here:

```
CaddrLens - lens which focuses at CDR, then at CDR and CAR at last of initial value.

CaddrLens
   lens-c  = CDR-LENS . CDR-LENS . CAR-LENS
                |             |       |
                |             |       |
                |             +---+   |
                | (CONS 1         |   |
                +-->  (CONS 2     V   V
                                (CONS 3 (CONS 4 NIL))))

lens-p = (list                                           ^
            <lens with focus at CAR: CarLens> ---------> 1
            <lens with focus at CADDR: CaddrLens>        2
            )                                 +--------> 3
                                                         4
# -> (list 1 3)
```

Quick recap for `(lens-c)` and `(lens-p)` utility functions: they both construct complex lenses from more elementary ones, but `(lens-c)` should be used in order to access _deeper into structure_ but `(lens-p)` should be used to access to multiple places at the same time.

And as the high level abstraction for working with lenses is _lens isomorphism_. To be short, it is a mapping between two lenses which able to move data from whole value to another with respect of thier inner structure.

```picolisp
(let (CarLens (car-lens)
      CdrLens (cdr-lens)
      CadrLens (lens-c CdrLens CarLens)
      CaddrLens (lens-c CdrLens CdrLens CarLens)
      CadddrLens (lens-c CdrLens CdrLens CdrLens CarLens)
      Car*CadrLens (lens-p CarLens CadrLens)
      Caddr*CadddrLens (lens-p CaddrLens CadddrLens))
    (lens-iso-1to1
        Caddr*CadddrLens # pick third and fourth value
        '(1 2 3 4)       # here it will be 3 and 4
        Car*CadrLens     # and place them at positions 1 and 2
        '() ) )
# -> (3 4)
```

Lens isomorphism works in both ways: forward and backwards (bijection property) and always respects inner structures (i.e. builds homomorphic mapping).

Also its recommended to add _Prism property_ to declared lenses: all lenses should include special value to be used instead of NIL if focus with lens is failed. Its also called Omega-lens, and provide Omega value (i.e. default value) for faulty focus in lens constructor:

```picolisp
(lens-get (car-lens 'DEFAULT) NIL)
# -> DEFAULT
```

As you can see, programming with lenses is very abstract and declarative. Try to research more insights in your data structure and implement own elementary lenses to be able to work with different formats of your data.

# Usage

## Utility functions

* `(lens-get L S)` focus at given point with lens `L` in structure `S` and return focused value
* `(lens-put L A S)` focus at given point with lens `L` in structure `S` and replace/insert value `A` at focused place and return updated _whole_ value
* `(lens-map Fun L S)` focus at given point with lens `L` in structure `S`, update value with `Fun` function and return updated _whole_ value
* `(lens-c L1 ...)` lens composition constructor, composes provided in args lenses in one complex lens
* `(lens-p L1 ...)` lens product constructor, builds new complex lens which collects/scatters data to/from list of values and list of different focus points provided by different lenses
* `(l-fmap Fun C)` functor fmap mapper, updates with `Fun` function wrapped value in `C` constructor
* `(lens-iso-1to1 Lfrom Sfrom Lto Sto)` lens isomorphism function. Focuses at places with `Lfrom` lenses in `Sfrom` whole value, extract values from focus points, navigates places in `Sto` structure with lens `Lto` and places extracted values at places provided by `Lto`
* `(l-make-lens Omega Env . LensBody)` lens implementation helper. `Omega` value will be used in case of faulty focus. `Env` contains result of `(env)` function for correct capture of environment for correct execution of lens. `LensBody` is spliced into fargs lambda expression.

## Lenses constructors

* `(car-lens [Omega])` lens with focus at CAR of list/cons-pair. If Omega value is provided then it will be used instead of NIL
* `(cdr-lens [Omega])` lens with focus at CDR of list/cons-pair. If Omega value is provided then it will be used instead of NIL
* `(at-lens I [Omega])` lens with focus at given position `I` of list. If Omega value is provided then it will be used instead of NIL
* `(assoc-at-lens Key [Omega])` lens with focus at value for given key `Key` in association list. If Omega value is provided then it will be used instead of NIL
* `(traverse-lens)` special lens which focuses at each element of list in current context

# Examples

## Data access by lenses

```picolisp
# Always call lens constructor - it will always return clojure with lens logic
(car-lens 1)
# -> ((F S) (bind '((Omega . 1)) (let Rst (cdr S) (l-fmap '((V) (cons V Rst)) (F (or (car S) Omega))))))

# pass lens instance to lens-{get,put,map} functions to access focus value
(lens-get (car-lens) '(1 2 3))
# -> 1

(lens-put (car-lens) 1 '(0 2 3))
# -> (1 2 3)

(lens-map reverse (car-lens) '((1 2) (3 4)))
# -> ((2 1) (3 4))
```

## Deep navigation with lenses

```picolisp
# combining multiple lenses to navigate further into data is possible with ``lens-c`` utility function.

# we need to put "2" value after "1" at the same level
(setq InData '((A ((1)))))

(lens-put
   (lens-c
      (assoc-at-lens 'A)
      (car-lens)
      (car-lens)
      (cdr-lens) )
   2
   InData )
# -> ((A ((1 . 2))))
```

## Multiple lenses union

```picolisp
# access multiple focus points in data structure at once with ``lens-p`` utility function.

# we need to increment all counters in our document
(lens-map
   '((L) # because ``lens-p`` focuses at multiple places,
         # L there will be bound to list of all focused values - counters for update
      (mapcar inc L) )
   (lens-p
      (assoc-at-lens "countCustomers")
      (assoc-at-lens "countMaleCustomers")
   '(("countCustomers" . 1) ("countMaleCustomers" . 1)) ) )
# -> (("countMaleCustomers" . 2) ("countCustomers" . 2))
```

## Lens isomorphism

```picolisp
(lens-iso-1to1
   (lens-c (cdr-lens) (car-lens))
   '(0 1 0)
   (car-lens)
   '(0 2 3) )
# -> (1 2 3)

# And if we swap last two arguments with first ones then this operation will be reversed - inverse morphism
(lens-iso-1to1
   (car-lens)
   '(0 2 3)
   (lens-c (cdr-lens) (car-lens))
   '(0 1 0) )
# -> (0 0 0)
```

## Own lens definition

Lets introduce our own CAAR lens:

```picolisp
(de caar-lens (Omega) # its a good practice to include Omega values to turn your lenses to prisms in case of failed focus
   (l-make-lens Omega NIL
   # first argument is Omega value which will be fixed in environment of lens body and
   # second argument contains additional assoc list of fixed environment for other variables
   # which can be get by ``env`` function or by building it by hand

      # lens body lies next
      (F S)
      # lens arguments: functor to apply to focused value and S - whole value
      (l-fmap
         # lens are follow functors constraints so always return result of l-fmap function
         '((V) (cons (cons V (cdar S)) S)) # how to change the whole value
         (F (or (caar S) Omega)) ) ) ) # how to modify focused value.
                                       # Its good practice to include Omega value fallback in
                                       #  order to recover when focus is failed
```

If you defined your lens dont forget to test it properly especially for following lens laws. Utility functions for law-checking are included in [test.l](tests/test.l).

## Application example

Lets assume that we have to request data from some API and we have to reformat response structure in order to match our application terms. As we develop our hypothethical service we always know the format and structure of data we are working on, even if its provided from external sources. Lets say, that API which are needed for us provides data in this JSON format:

```json
[{
    "id": <number>,
    "items": [
        {
            "sub_id": <number>,
            "name": <string>,
            "relations": [<numbers>]
        }
    ]
}]
```

With our knowledge of this JSON document schema we construct the lens to extract fields from input document:

```picolisp
(setq InLens
   (lens-c # (1)
      (car-lens) # array is at the top level of document -> step into it
      (lens-p # (2)
         (assoc-at-lens "id")
         (lens-c # (3)
            (assoc-at-lens "items")
            (car-lens) # just assume that our legacy API in ``items`` key contains only one element
            (assoc-at-lens "sub_id") )
         (lens-c (assoc-at-lens "items") (car-lens) (assoc-at-lens "relations")) ) ) ) # we want to "pull" ``relations`` key at topmost level
```

We use `lens-c` in order to step further into deeper structure (i.e. into "items" key) and use `lens-p` to gather data from multiple points of interest into single structure. Next, lets say that we are actually using objects of this format in our application:

```picolisp
# document structure
(("myId" . <number>)
 ("elements" (("elementId" . <number>)))
 ("subRelations" (<number1> <number2> ...)))
```

As you may see, there is one interesting change for our application document format: "subRelations" key is node pulled off from nested structure to the topmost level of struct. Then we cant include navigation to key "relations" in context of `lens-c` at (3) mark, because if we do then extracted "relations" value will be stored in context of (3) nested object as it does by (3) lens composition:

```
CURRENT CONTEXT -> value for key "items" -> first element -> value for key "sub_id"
```

Where _current content_ is defined by closest `lens-p` because it is not traverses _deeper_ into structure but preserves "memory" of current reached level to collect/insert data. So, if we need to pull data from deep nested structure then we should navigate and focus with lenses at the context of wanted level to extract. In this example, "subRelations" key is at the same level as "myId" in internal document structure so in `InLens` we focus to needed key at the same level as accessing to "id" field at topmost level of document from external service. `lens-p` at (2) mark is used for accessing multiple keys at once when lenses navigated to first document in array as first lens in (1) `lens-c` already did.

Now its time to put our extracted data into our structure. Lets define lens for internal document:

```picolisp
(setq OutLens
   (lens-p # put multiple values at once
      (assoc-at-lens "documentId")
      (lens-c # "elements" node is composed one
         (assoc-at-lens "elements") # step into key "elements"
         (car-lens) # first element of sublist
         (assoc-at-lens "elementId") )
      (assoc-at-lens "subRelations") ) ) # put pulled up value from "relations" key from external service format
```

With two defined lenses to extract and to compose data we can build isomorphism between them:

```picolisp
(load "@lib/json.l")

(setq InJSON
   (parseJson
      "[{\"id\": 1, \"items\": [{\"sub_id\": 2, \"relations\": [10, 11, 12]}]}]" ) )

(lens-iso-1to1 InLens InJSON OutLens NIL)
# -> (("subRelations" 10 11 12) ("elements" (("elementId" . 2))) ("documentId" . 1))
```

# Testing

Library comes with unit tests ([link](https://github.com/aw/picolisp-unit)). To run tests, exec:
```bash
make check
```

# Contributing

Currently this library in state of development so if you want to help to improve it, please feel free either to create an issue or open MR.

# License

[MIT License](LICENSE.md)

# Maintainer

Nail Gibaev, <abel.ze.normand@gmail.com>
