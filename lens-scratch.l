(de l-make-idf @
   # identity constructor
   (pass cons 'idf) )

(de l-make-constf @
   # constant constructor
   (pass cons 'constf) )

# fmap: (a -> b) -> f a -> f b
(de l-fmap ("Fun" "C")
   # functor mapping implementation
   # ARGS:
   #   Fun -- function for modifying FOCUSED value
   #   C -- cons pair with functor symbol in CAR and value in CDR
   # RETURN:
   #   changed value by Fun and wrapped in constructor
   (let Res (case (car "C")
               (constf "C")
               (idf (l-make-idf ("Fun" (cdr "C")))) )
      (====)
      Res ) )

(de lens-map ("Fun" "L" "S")
   # ARGS:
   #   Fun -- function for modifying FOCUSED value
   #   L -- lens instance
   #   S -- WHOLE value
   # RETURN:
   #   apply Fun on focused value by lens L for S WHOLE value and return S WHOLE value changed
   (let (Fun "Fun"
         Fmap '(("A")
               # A will be focused value
                (l-fmap Fun (l-make-idf "A")) )
         Res (cdr ("L" Fmap "S")) )
      # Fmap function manages for unwrapping FOCUSED value by lens, passing it to ``fmap`` and
      # wrapping it back

      # lens execution result will be wrapped in one of the constructors so unwrap them manually
      (====)
      Res ) )

(de lens-get ("L" "S")
   # ARGS:
   #   L -- lens instance
   #   S -- WHOLE value
   # RETURN:
   #   FOCUSED value provided by lens L for S WHOLE value
   (let (Fmap '(("A")
                # A will be focused value
                (l-fmap NOFUN (l-make-constf "A")) )
         Res (cdr ("L" Fmap "S")) )
      (====)
      Res ) )

(de lens-put ("L" "A" "S")
   # ARGS:
   #   L -- lens instance
   #   A -- value to put at FOCUS point
   #   S -- WHOLE value
   # RETURN:
   #   WHOLE value with new value A at FOCUS of lens L
   (let (A "A"
         Fmap '((_IGNORE)
                # because there is no need to manage data which will be placed at lens FOCUS
                # so just return FOCUSED
                A )
         Res (lens-map Fmap "L" "S") )
      (====)
      Res) )

(de lens-iso-1to1 (L1 S1 L2 S2)
   (lens-put L2 (lens-get L1 S1) S2) )

(de lens-c (Lenses)
   (let L Lenses
      '((Fmap S)
        (l-c-dot (reverse Lenses) Fmap S) ) ) )

(de l-c-dot (Lenses F S)
   (let (L (car Lenses)
         Rst (cdr Lenses) )
      (if Rst
         (l-c-dot '((X) (L F X)) S)
         (L F S) ) ) )

##########
# lenses #
##########

(de car-lens ("Om")
   # ARGS:
   #   Om -- OMEGA value for replacing NIL value, if focus is failed
   # RETURN:
   #   instance of lens
   '(("F" "S")
     # F -- function for wrap-apply-unwrap management
     # S -- WHOLE value
     (let Rst (cdr "S")
        (l-fmap
           '(("V") (cons "V" Rst))
           ("F" (or (car "S") "Om")) ) ) ) )

(de cdr-lens ("Om")
   '(("F" "S")
     (let Car (car "S")
        (l-fmap
           '(("V") (cons Car "V"))
           ("F" (or (cdr "S") "Om")) ) ) ) )

##############
# LAZY TESTS #
##############

(de identity (X) X)
(de plus50 (X) (+ X 50))

(setq CarLens (car-lens))
(setq CdrLens (cdr-lens))

(println (lens-map plus50 CarLens '(1 2 3)))
(println (lens-put CarLens 0 '(1 2 3)))
(println (lens-get CarLens '(1 2 3)))
(println (lens-put CdrLens '(4 5) '(1)))

(println (lens-iso-1to1 CarLens '(1 2 3) CarLens '(4 5)))
(println (lens-iso-1to1 CdrLens '(1 2 3) CdrLens '(4 5)))