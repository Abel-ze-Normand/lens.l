(load "lens.l")
(load "tests/unit.l")

(de plus50 (X) (+ X 50))
(de identity (X) X)

(de assert-lens-omega (L Omega . @)
   (list
      'assert-equal (lit Omega)
      (list 'lens-get
         (conc (list L) (mapcar lit (rest)) (list (lit Omega)) )
         NIL )
      (text "[@1] OMEGA AUTOTEST: lens uses Omega value when focus is not possible" L) ) )

(de assert-lens-getput-law (L . @)
   (let (S '((A . 1) (B . 2))
         Lens (conc
                 (list L)
                 (mapcar lit (rest)) ) )
      (list 'assert-equal (lit S)
         (list 'lens-put Lens
            (list 'lens-get Lens (lit S)) (lit S) )
         (text "[@1] GETPUT LAW AUTOTEST: if yielded value from lens-get for lens and whole value S put back with the same lens then result value of lens-put should be equal to S" L) ) ) )

(de assert-lens-putget-law (L InitS A . @)
   (let Lens (conc
                (list L)
                (mapcar lit (rest)) )
      (list 'assert-equal (lit A)
         (list 'lens-get
            Lens
            (list 'lens-put Lens (lit A) (lit InitS)) )
         (text "[@1] PUTGET LAW AUTOTEST: if put data A into S with lens and use the same lens to retrieve focused value then it should be A" L) ) ) )

(de assert-lens-putput-law (L InitS A1 A2 . @)
   (let Lens (conc
                (list L)
                (mapcar lit (rest)) )
      (list 'assert-equal (lit A2)
         (list 'lens-get
            Lens
            (list 'lens-put Lens (lit A2)
               (list 'lens-put Lens (lit A1) (lit InitS)) ) )
         (text "[@1] PUTPUT LAW AUTOTEST: if with lens put data A into S and put data B into same S then result whole value at focus of lens must be equal to B" L) ) ) )

[execute
   '(assert-equal '(51 2 3)
       (lens-map plus50 (car-lens) '(1 2 3))
       "lens-map by given mapping function focuses and modifies CAR of list" )

   '(assert-equal '(0 2 3)
       (lens-put (car-lens) 0 '(1 2 3))
       "lens-put updates value in given place provided by lens" )

   '(assert-equal 1
       (lens-get (car-lens) '(1 2 3))
       "lens-get retrieves value at given place provided by lens" )

   '(assert-equal '(1 2 3)
       (lens-put (cdr-lens) '(2 3) '(1))
       "lens-put with ``cdr-lens`` puts new CDR of list" )

   '(assert-equal '(CODOMAIN1 DOMAIN2)
       (lens-iso-1to1 (car-lens) '(CODOMAIN1 CODOMAIN2 CODOMAIN3) (car-lens) '(DOMAIN1 DOMAIN2))
       "lens isomorphism (car-lens <-> car-lens) retrieves CAR from original data structure and moves it to CAR of target data structure" )

   '(assert-equal '(DOMAIN1 CODOMAIN2 CODOMAIN3)
       (lens-iso-1to1 (cdr-lens) '(CODOMAIN1 CODOMAIN2 CODOMAIN3) (cdr-lens) '(DOMAIN1 DOMAIN2))
       "lens isomorphism (cdr-lens <-> cdr-lens) retrieves CDR from original data structure and moves it to CDR of target data structure" )

   '(assert-equal 1
       (lens-get (lens-c (car-lens) (cdr-lens)) '((A . 1)))
       "lens composition focuses deeper into data structure" )

   '(assert-equal '((A . 2) PLACEHOLDER 2)
       (lens-iso-1to1
          (lens-c (cdr-lens '(PLACEHOLDER)) (car-lens))
          '(((NEWVAL)))
          (lens-c (cdr-lens) (car-lens))
          '((A . 2) 1 2) )
       "lens isomorphism with lens composition does not lost scopes and correctly maps data to target WHOLE value" )

   '(assert-equal '(DOMAIN1 DOMAIN2 CODOMAIN1)
       (lens-iso-1to1
          (at-lens 1)
          '(CODOMAIN1 CODOMAIN2 CODOMAIN3)
          (at-lens 3)
          '(DOMAIN1 DOMAIN2 DOMAIN3) )
       "lens isomorphism (at-lens1 <-> at-lens3) picks value from CAR of list and places it into CADDR of list" )

   '(assert-equal '(1 2)
       (lens-get (lens-p (car-lens) (lens-c (cdr-lens) (car-lens))) '(1 2))
       "lens product retrieves multiple values from list of lenses and collect them back into list" )

   '(assert-equal '(2 1)
       (lens-iso-1to1
          (lens-p (car-lens) (lens-c (cdr-lens) (car-lens)))  # pick #1 and #2 value
          '(1 2)
          (lens-p (lens-c (cdr-lens) (car-lens)) (car-lens))  # #1 value to CADR and set #2 value to CAR
          '() )
       "lens product preserves order of lenses list" )

   '(assert-equal '(1 2 3)
       (lens-get (lens-c (traverse-lens) (cdr-lens)) '((A . 1) (B . 2) (C . 3)))
       "traversal lens focuses at each element of list" )

   # well behaving lenses follows 3 laws:
   # a) GetPut: A = get LENS1 (put LENS1 A S)
   # b) PutGet: S = put LENS1 (get LENS1 S) S
   # c) PutPut: B = get LENS1 (put LENS1 B (put LENS1 A S))

   (assert-lens-omega 'car-lens 'DEFAULT)

   (assert-lens-omega 'cdr-lens '(DEFAULT))

   (assert-lens-omega 'at-lens 'DEFAULT 1)

   (assert-lens-omega 'assoc-at-lens 'DEFAULT 'KEY)

   (assert-lens-getput-law 'car-lens)

   (assert-lens-getput-law 'cdr-lens)

   (assert-lens-getput-law 'at-lens 1)

   (assert-lens-getput-law 'assoc-at-lens 'A)

   (assert-lens-putget-law 'car-lens NIL 1)

   (assert-lens-putget-law 'cdr-lens '(a . b) 'c)

   (assert-lens-putget-law 'at-lens '(a a a) 'c 1)

   (assert-lens-putget-law 'assoc-at-lens '((A . 1) (B . 2)) 3 'C)

   (assert-lens-putput-law 'car-lens NIL 1 2)

   (assert-lens-putput-law 'cdr-lens '(a . b) 'c 'd)

   (assert-lens-putput-law 'at-lens '(a a a) 'b 'c 1)

   (assert-lens-putput-law 'assoc-at-lens '((A . 1)) 2 3 'A)]