# Explanation of lens.l implementation

This document contains couple of topics presented in QA format in order to understand for contributors/code reviewers motivation behind current implementation.

# Why do mess up with idf/constf constructors for l-fmap function? Why not use plain values without wrapping?

There is two things that should be followed:

1) Functor `fmap` definition

If we recall functor map declaration `fmap :: (a -> b) -> f a -> f b` then you may see that not only mapping function is provided to `fmap` (`a -> b` in function declaration means that `fmap` expects some function that gets some value `a` as argument and returns completely new value `b` as result), but also `f a`. What does that mean? Actually, `f a` as argument means that value may  be encapsulated in some structure (`f` there used as general carrier of value). For example if we say that we pass to `fmap` input value `(list 1)` then `f` will be `list` - our values `(1, )` which are bound to `a` are organised into generalized structure `f` which is `list`. At last, if you refer to `fmap` declaration it returns instance of `f b` which wraps all constraints for `fmap`: returned value from `fmap` should be instance of the _same structure_ `f` as was passed to input but with placed _new_ value inside.

2) Separation between updates and data retrieval

Most of the time used `idf` (identity functor) constructor - it is just tells us "i hold some value so just work with it and return again incapsulated in idf". And because `fmap` is generalized over set of possible operations with input data, `idf` applies mapping function over its value and returns new copy of updated value as `idf` instance. But there is small issue with `put` semantics to update nested value inside struct - there is no need to apply any mapping functions when you need to place value inside structure, just put it down and forget about it. So there is needed `constf` (constant functor) to guarantee that data would not be updated by mapping function.

# How does lens in lens.l actually work?

Lenses are composed into clojures with arguments `(F S)` where `F` is special function which somehow manages focused value and wraps results into `idf`/`constf` constructors and `S` is whole value. Rule of thumb is for lenses definition is to return result of lenses clojure result of `l-fmap` call. For example lets check `(car-lens)` implementation:

```picolisp
(de car-lens (Omega)
   # lens for focusing at CAR
   # ARGS:
   #   Omega -- OMEGA value for replacing NIL value, if focus is failed
   # RETURN:
   #   instance of lens
   (l-make-lens Omega NIL
      (F S)
      # F -- function for wrap-apply-unwrap management
      # S -- WHOLE value
      (let Rst (cdr S)
         (l-fmap
           '((V) (cons V Rst)) # how to reconstruct whole value with new updated focused value
           (F (or (car S) Omega)) ) ) ) ) # argument of F contains focused value
```

There is used `l-make-lens` helper function but actually it does little: only catches up environment of variables which scope should be captured explicitly (I will elaborate about scopes issue for PicoLisp further down) and constructs new lambda from its body.

Inspection of `F` function inside of clojure gives us results:

```picolisp
: (lens-get (car-lens) '(1 . 2))
# enter debugger
(F (or (car S) Omega))
! F
-> ((A) (l-fmap NOFUN (l-make-constf A)))
```

`F` holds special function which are declared in `(lens-get)`, `(lens-put)` and `(lens-map)`. As you may see, `(lens-get)` function does nothing with focused data and returns it without modifications so constant functor best suited here. But lets check with the same breakpoint `F` function for `(lens-map)`:

```picolisp
: (lens-map '((X) (+ X 1)) (car-lens) '(1 . 2))
(F (or (car S) Omega))
! F
-> ((A) (l-fmap '((X) (+ X 1)) (l-make-idf A)))
```

`(lens-map)` passes my lambda to `fmap` mapping function with identity constructor: that means that focused value will be updated with my lambda and result will be put back with recostruction functions. `(lens-put)` is also a `(lens-map)` but with fixed return value:

```picolisp
: (lens-put (car-lens) 2 '(1 . 2))
(F (or (car S) Omega))
! F
-> ((A) (l-fmap '((_IGNORE) (bind '((A . 2)) A)) (l-make-idf A)))
```

To sum up, all lenses utilities are built around utilities themselves and use of `(l-fmap)` applications to data in order apply some transformation to focused value or to navigate to focused value. Functions composition is also possible because of backtracking of `F` functions applications for each lens which are navigate to focus inside into structure, modify with some function and construct all structures along the path backwards from inside. This nice feature is used for lenses composition and they will be discussed further in this document.

# How does lens isomorphism works?

Lens isomorphism is a nice way to build relationships between structures of different objects (also you may say _with different topologies_). Isomorphism from algebra is homomorphism with bijection property. Homomorphism, to be short, is a some abstract morphism which saves objects structures upon mapping. It is a most important part in understanding lens isomorphism. Bijection in practice is not always applicable (because not all lenses are well behaved ones), but if bijection is possible to implement -- then there is nice feature to management of your data, because you can actually apply isomorphism in two ways:

```picolisp
(let (R1 (lens-iso-1to1 L1 S1 L2 NIL)
      R2 (lens-iso-1to1 L2 S2 L1 NIL) )
   (assert (= R1 S2))
   (assert (= R2 S1)) )
```

Basically there is a common mistake which I did myself too. Sometimes with definition of "from" lens you may write lens with thought that you want to repeat "from" whole value internal structure. But its not the case. You actually should have much more attention to _where_ put your extracted data and how its possible to access in "to" lens.

Lets check example from README. At first I wrote this "from" lens in order to access values:

```picolisp
(setq InLens
   (lens-c
      (car-lens)
      (lens-p # 3
         (assoc-at-lens "id")
         (lens-c
            (assoc-at-lens "items")
            (car-lens)
            (lens-p # 1
               (assoc-at-lens "sub_id")
               (assoc-at-lens "relations") ) ) ) ) ) # 2
```

Here I just repeated structure of JSON of our hypothethical external API and composed lenses in this manner. But with definition "to" lens I met with the problem that I can not actually put "subRelations" into my "to" whole value structure. Lets check how we extract values with this lens:

```picolisp
(lens-get InLens InJSON)
-> (1 (2 (10 11 12)))
```

Whoops, when I defined `InLens` I actually made my work not so much easier. What happened? Well, there is a problem with `(lens-p)` at #1 mark in `InLens` definition. `(lens-p)` stops to "digging" into structure levels and starts to expand to different focus values with fixed context where `(lens-p)` was called (in the "items" key and for "car" of "items" list). So lens at #2 mark is accessing value _in context_ of #1 marked `(lens-p)`. And all extracted values for "sub_id" and "relations" keys was placed at the same level as `(lens-p)` declared.

But if we refer to "to" whole value structure then you may see that "relations" value is pulled to upper level. In order to achieve this I have to _remove_ "relations" value access from context of #1 `(lens-p)` (which is focused to first value of "items" key and will focus other lenses in context of this level) to level of topmost `(lens-p)` which is at mark #3. So correct "from" lens will be:

```picolisp
(setq InLens
   (lens-c
      (car-lens)
      (lens-p
         (assoc-at-lens "id")
         (lens-c
            (assoc-at-lens "items")
            (car-lens)
            (assoc-at-lens "sub_id") )
         (lens-c (assoc-at-lens "items") (car-lens) (assoc-at-lens "relations")) ) ) ) # we pulling this values to wanted level for our "to" whole value format
```

And isomorphic "to" lens to build "to" whole value structure is defined then as it should be intuitively:

```picolisp
(setq OutLens
   (lens-p
      (assoc-at-lens "documentId")
      (lens-c
         (assoc-at-lens "elements") # step into key "elements"
         (car-lens) # first element of sublist
         (assoc-at-lens "elementId") )
      (assoc-at-lens "subRelations") ) )
```

Also keep in mind to keep track for count of lenses inside all `(lens-p)` lens utility functions. They should match at all of levels of "digging" into structure. For example, topmost `(lens-p)` in `InLens` contains 3 lenses ("id", navigation to "items" and further and extractor of nested "relations" key to topmost level) so then `OutLens` topmost `(lens-p)` contains 3 lenses to put data into "to" structure.

# Whats overhead for lens-c and lens-p functions?

TL;DR: Theoretically, not that much. I did not synthetic benchmarks yet but I think there is no drastic overhead.

## Lens composition

This function chains multiple lenses into bigger one. But body of `(lens-c)` is generated at the moment of call `(lens-c)`. It expands all lenses definitions and stack them like functional composition in algebra does:

```
f . g . h <-> h(g(f))
```

Lens composition utility function builds new complex lens with fixed environments for each lens and with tracking of intermediate results in order to rebuild whole value structure after focused value is updated:

```picolisp
: (pretty (lens-c (car-lens) (car-lens)))
((Fun S)
   ('((F S) # second car-lens
         (bind '((Omega))
            (let Rst (cdr S)
               (l-fmap
                  '((V) (cons V Rst))
                  (F (or (car S) Omega)) ) ) ) )
      '((X)
         ('((F S) # innermost executed first -> its first car-lens
               (bind '((Omega))
                  (let Rst (cdr S)
                     (l-fmap
                        '((V) (cons V Rst))
                        (F (or (car S) Omega)) ) ) ) )
            Fun
            X ) )
      S ) )-> " )"
```

## Lens product

Lens product utility function works in the same manner as lens composition utility function with internal cycles which are iterate over all lenses in `(lens-p)` body and compact them in two clojures for `(l-fmap)` call - second one is getter chain which accumulates lenses results at the current level into list and first of them executes multiple PUT's into multiple focus points:

```picolisp
: (pretty (lens-p (car-lens) (car-lens)))
((Fun S)
   (bind (env '(S Fun))
      (l-fmap
         '((Xs)
            (let Lenses
               '(((F S)
                     (bind '((Omega))
                        (let Rst (cdr S)
                           (l-fmap
                              '((V) (cons V Rst))
                              (F (or (car S) Omega)) ) ) ) )
                  ((F S)
                     (bind '((Omega))
                        (let Rst (cdr S)
                           (l-fmap
                              '((V) (cons V Rst))
                              (F (or (car S) Omega)) ) ) ) ) )
               (bind (env '(S Xs))
                  (for L Lenses # PUT multiple values and refresh whole value
                     (setq S (lens-put L (car Xs) S))
                     (setq Xs (cdr Xs)) )
                  S ) ) )
         (Fun
            (list # retrieve values from different points of view and collect them in one list
               (lens-get # first focus point
                  '((F S)
                     (bind '((Omega))
                        (let Rst (cdr S)
                           (l-fmap
                              '((V) (cons V Rst))
                              (F (or (car S) Omega)) ) ) ) )
                  S )
               (lens-get # second focus point
                  '((F S)
                     (bind '((Omega))
                        (let Rst (cdr S)
                           (l-fmap
                              '((V) (cons V Rst))
                              (F (or (car S) Omega)) ) ) ) )
                  S ) ) ) ) ) )-> " )"

```

# Why do you build clojures by hand? Why not just `'((X) <lambda body>)`?

Working with clojures in PicoLisp is possible but not such straightforward as you may remember from experience in other languages. The problem is that most powerful feature of clojures is _capturing of current context_ where clojure is created. In languages with lexical scope its abstracted from programmer because at clojure instantiation time variables in clojure that are defined in current lexical scope are captured in own environment for each clojure. But in PicoLisp there is no scopes for each function or clojure because all symbols values are stored in single global lookup table and their values are looked up at runtime by symbols identifier in general. [Alexander Buerger already warned you about this](https://software-lab.de/doc/faq.html#closures). So, if keeping track of variable bindings for clojure construction is not provided by interpreter then you should do it yourself with explicit `(bind)` calls to "freeze" the environment of runtime and build clojures with explicit lists because only explicit `(list)` call evaluates its own arguments but shorthand `'((X) <lambda>)` does not (because its just quoted S-expression and one does not copy values of symbols used in clojure nor environment of variables upon creation). To be fair, its not a big deal IMO but I actually found this pretty interesting for application development. And true homoiconity of PicoLisp is actually great feature, with homoiconity I don't need to work with macroses and perform clubersome debugging of their expansion when I can just check my built S-expression at runtime.

# Does PicoLisp actually need lenses?

Why not? :) Well, I'm actually think that its nice abstraction in general when you have growing system or service and you want to save declarative nature of your code. But there is an opinion in PicoLisp community to keep it very simple and with extensive use of default functions which are overwhelmingly fast. I understand this decision. But I still think that you need to build your application code around succinct abstractions which should be used as stepping stones in order to defeat complexity of our programs.

# What pros and cons are lenses provide versus `(get)` standard function for objects?

`(get)` works only with proplist of symbol and lists by lookup algorithm. `(get)` function and `(lens-c)` are pretty similar but lenses are more generalized and can extend `(get)` function's functionality.
